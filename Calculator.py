def add(x, y): 
    """Функція додавання"""
    return x + y


def subtract(x, y):
    """Функція віднімання"""
    return x - y


def multiply(x, y):
    """Функція множення"""
    return x * y


def divide(x, y):
    """Функція ділення"""
    return x / y


n = True

while n == True:

    s = input(""" Яку операцію виконати?\n + додавання \n - віднімання \n * множення \n / ділення  
    \n Пробіл - вихід з програми \n --> """)
    if ord(s) != 32:
        a = float(input("Введіть число a: "))
        b = float(input("Введіть число b: "))
        if s in '+':
            result = add(a, b)
            print("Результат додавання:", result)
        elif s in '-':
            result = subtract(a, b)
            print("Результат віднімання:", result)
        elif s in '*':
            result = multiply(a, b)
            print("Результат множення:", result)
        elif s in '/':
            result = divide(a, b)
            print("Результат поділу:", result)
    else:
        print("До побачення!")
        n = False
        break




